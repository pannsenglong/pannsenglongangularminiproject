import { AfterContentInit, Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { BookService } from 'src/app/services/book.service';
import Swal from 'sweetalert2';
import { ImageServiceService } from '../../services/image.service';
import { BookDetailComponent } from '../book-detail/book-detail.component';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css'],
})
export class EditBookComponent implements OnInit {
  constructor(
    private imageService: ImageServiceService,
    private http: HttpClient,
    private bookService: BookService,
    private route: Router,
    private routeNavigate: ActivatedRoute
  ) {}

  defaultImage = '../../assets/default-book.png';
  myForm: any;
  selectedFile: any;

  tit: any;
  aut: any;
  des: any;
  thum: any;
  imageSrc: any;
  catId: number;

  id: any;

  book: Book;

  oldBookData: Book;

  ngOnInit() {
    console.log(this.selectedFile);

    this.id = this.routeNavigate.snapshot.paramMap.get('id');

    this.bookService.getOneBook(this.id).subscribe((res) => {
      this.oldBookData = res;
      this.tit = this.oldBookData.title;
      this.aut = this.oldBookData.author;
      this.des = this.oldBookData.description;
      this.thum = this.oldBookData.thumbnail;
    });

    this.myForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      author: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      file: new FormControl(''),
    });
  }

  onFileChange(event: any) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      this.selectedFile = <File>event.target.files[0];

      console.log(this.selectedFile);

      reader.readAsDataURL(this.selectedFile);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.oldBookData.thumbnail = this.imageSrc
      };
    }
  }

  submit() {
    console.log(this.selectedFile);
    
    if (this.selectedFile !== undefined) {
      this.imageService.postFile(this.selectedFile).subscribe((res) => {
        console.log(res.imageUrl);

        this.thum = res.imageUrl;
        console.log(this.thum);

        this.book = {
          id: '',
          title: this.tit,
          author: this.aut,
          description: this.des,
          thumbnail: this.thum,
          category: { id: 1, title: '' },
        };

        this.bookService.updateBook(this.id, this.book).subscribe((res) => {
          console.log(res);
          if (res) {
            const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger',
              },
              buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire(
              'Successfully!',
              'Your book has been updated sucessfully.',
              'success'
            );
            this.route.navigate(['/home']);
          }
        });
      });
    }else{
      
      this.book = {
        id: '',
        title: this.tit,
        author: this.aut,
        description: this.des,
        thumbnail: this.thum,
        category: { id: 1, title: '' },
      };

      this.bookService.updateBook(this.id, this.book).subscribe((res) => {
        console.log(res);
        if (res) {
          const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger',
            },
            buttonsStyling: false,
          });
          swalWithBootstrapButtons.fire(
            'Successfully!',
            'Your book has been updated sucessfully.',
            'success'
          );
          this.route.navigate(['/home']);
        }
      });
    }
    console.log(this.book);
  }

  get title() {
    return this.myForm.get('title');
  }

  get author() {
    return this.myForm.get('author');
  }

  get description() {
    return this.myForm.get('description');
  }

  get file() {
    return this.myForm.get('file');
  }

  onTitleChange(event: any) {
    this.tit = event;
  }

  onAuthorChange(event: any) {
    this.aut = event;
  }

  onDescriptionChange(event: any) {
    this.des = event;
  }
}
