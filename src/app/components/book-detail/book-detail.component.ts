import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { param } from 'jquery';
import { Book } from 'src/app/models/book';
import { BookService } from '../../services/book.service';


@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  

  constructor(private route: ActivatedRoute, private bookService: BookService) {}

  id: any
  book: Book

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') 
    this.bookService.getOneBook 

    this.bookService.getOneBook(this.id).subscribe(res => {
      this.book = res
      console.log(this.book); 
    })

  }

}
