import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { AfterContentInit, Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  constructor( private bookService: BookService, private router: Router) { }

  books=[{id: "" , title: "", author: "", description: "", thumbnail: ""}]


  ngOnInit(): void {
    this.getBooks();
  }


  getBooks(): void {
    this.bookService.getBooks().subscribe((res: any)=>{
      this.books = res._embedded.books;
      console.log(this.books);
    })
  }


  goToBookDetail(id: any): void {
    this.router.navigate(['/book', id])
    console.log(id);
  }

  goToAddBook(): void{
    
    this.router.navigate(["/add-book"])
  }

  goToEditBook(id: any): void {
    this.router.navigate(['/edit-book', id])
  }

  


  deleteBook(id: any): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.bookService.deleteBook(id).subscribe(() =>
        (this.books = this.books.filter((item) => item.id !== id))
      )
      
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: 'Book delete successfully'
      })
      }
    })  
  }
  

  


}
