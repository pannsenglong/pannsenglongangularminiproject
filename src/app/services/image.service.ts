import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageServiceService {

  constructor(private http: HttpClient) { }

  BASE_URL = "http://localhost:8080/api/v1/upload"

  postFile(image: File){
    const uploadData = new FormData();
    uploadData.append('file', image);
    return this.http.post<any>(this.BASE_URL, uploadData);
  }
}
