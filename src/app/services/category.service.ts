import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  categories: Category
  BASE_URL = "http://localhost:8080/api/v1/categories/"


  getCategories(){
    return this.http.get<any>(this.BASE_URL)
  }

  postCategory(categoriry: Category){
    return this.http.post<Category>(this.BASE_URL, categoriry);
  }

  updateCategory(id: any, category: Category){
    return this.http.put<Category>(this.BASE_URL + `${id}`, category);
  }

  deleteCategory(id: any){
    return this.http.delete(this.BASE_URL  + `${id}`);
  }
}
