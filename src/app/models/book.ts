import { Category } from "./category";

export interface Book {
    id: any
    title:string; 
    author: string;
    description: string;
    thumbnail: string;
    category: Category
}
